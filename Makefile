
#Build directory
BUILD = build

#project name
#doesn't need to be associated with any file names
PROJ = baremetal

# Core frequency in Hz
F_CPU = 8000000

# Selecting Core
CORTEX_M=0

# Use newlib-nano. To disable it, specify USE_NANO=
#USE_NANO=--specs=nano.specs
USE_NANO=

# Use seimhosting or not
USE_SEMIHOST=--specs=rdimon.specs
USE_NOHOST=--specs=nosys.specs

CORE=CM$(CORTEX_M)
BASE=.

# Compiler & Linker
CC=arm-none-eabi-gcc
CXX=arm-none-eabi-g++
OBJCOPY=arm-none-eabi-objcopy
SIZE=arm-none-eabi-size

# Options for specific architecture
ARCH_FLAGS=-mthumb -mcpu=cortex-m$(CORTEX_M)

# Startup code
STARTUP=$(BASE)/startup/startup_ARM$(CORE).S

# -Os -flto -ffunction-sections -fdata-sections to compile for code size
CFLAGS=$(ARCH_FLAGS) $(STARTUP_DEFS) -DF_CPU=$(F_CPU) -Os -flto -ffunction-sections -fdata-sections
CXXFLAGS=$(CFLAGS)

# Link for code size
GC=-Wl,--gc-sections

# Create map file
MAP=-Wl,-Map=$(BUILD)/$(PROJ).map

STARTUP_DEFS=-D__STARTUP_CLEAR_BSS -D__START=main

LDSCRIPTS=-L. -L$(BASE)/ldscripts -T nokeep.ld
LFLAGS=$(USE_NANO) $(USE_NOHOST) $(LDSCRIPTS) $(GC) $(MAP)

SOURCES=$(wildcard source/**/*.c source/*.c)
OBJECTS=$(patsubst %.c,%.o,$(SOURCES))

all: dir $(BUILD)/$(PROJ).axf $(BUILD)/$(PROJ).elf $(BUILD)/$(PROJ).hex $(BUILD)/$(PROJ).bin size 
#all: dir $(BUILD)/$(PROJ).elf $(BUILD)/$(PROJ).hex $(BUILD)/$(PROJ).bin size 

#build axf file output (basically elf with DWARF debug info)
# $@ is shortcut for the target, $^ is shortcut for prereqs
#             TARGET: PREREQS
$(BUILD)/$(PROJ).axf: $(STARTUP) $(OBJECTS)
	$(CC) $^ $(CFLAGS) $(LFLAGS) -o $@

$(BUILD)/$(PROJ).elf: $(STARTUP) $(OBJECTS)
	$(CC) $^ $(CFLAGS) $(LFLAGS) -o $@

$(BUILD)/$(PROJ).hex: $(BUILD)/$(PROJ).elf
	$(OBJCOPY) -O ihex $^ $@

$(BUILD)/$(PROJ).bin: $(BUILD)/$(PROJ).elf
	$(OBJCOPY) -O binary $^ $@

dir:
	mkdir -p $(BUILD)

size:	$(BUILD)/$(PROJ).elf
	$(SIZE) -t $^

clean: 
	rm -rf $(BUILD)
	rm -f $(OBJECTS)
