#ifndef __NO_SYSTEM_INIT
//option to create your own C library system initialization
//Currently define __STARTUP_CLEAR_BSS when building to use
//initialization provided by startup_ARMCM0.S that zero's the BSS section
void SystemInit()
{}
#endif

//include target chip port definition library files

void main()
{
	//System is running at reset defaults
	
	//Default clock is in operation
	//Change system clock as needed
	//Initialize periphery clocks as needed
	
	//Initialize interupts
	
	//Initialize WDT, core features, etc
	
	//Initialize io, periphery, etc
	
	//Initialize board/system
	
	//main infinite loop
	for (;;);
}
